import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Pending } from 'src/app/models/pending'

import { Observable } from 'rxjs';




@Injectable({
  providedIn: 'root'
})
export class PendientesService {
  auth_token: String;
  
 
  private pendingUrl: string;
  constructor(
    private http: HttpClient

  ) { 
    this.pendingUrl = "/api/v1/milestones?project=";
  }

  public get(projId: String, auth_token: String): Observable<Pending[]> {
    const url = this.pendingUrl + projId;
    this.auth_token = auth_token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.auth_token,
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE'
        
      })
    };
    return this.http.get<Pending[]>(url,  httpOptions );
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from 'src/app/models/user';
import { Login } from 'src/app/models/login';

import { Observable} from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
  })
};

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private authUrl: string;

  constructor(
    private http: HttpClient
    ) { 
    this.authUrl = "/api/v1/auth";
  }

  public postLogin(userpwd: Login): Observable<User>{
    const url = this.authUrl;
    return this.http.post<User>(url, userpwd,  httpOptions );
  }
}

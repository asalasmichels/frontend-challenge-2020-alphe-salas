import { Component } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service'
import { PendientesService } from 'src/app/services/pendientes.service'

import { User } from 'src/app/models/user';
import { Login } from 'src/app/models/login';
import { Pending } from 'src/app/models/pending';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'frontend-challenge-2020-alphe-salas';
  myForm: FormGroup;
  user: User;
  loginData: Login;
  pendingList: Pending[] ;

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private pendienteService: PendientesService,
  ) {

 
  }

  ngOnInit(){
    this.myForm  = this.fb.group(
      { // phbhelloworld@gmail.com Nolodire20
        username: ['phbhelloworld@gmail.com', Validators.required],
        password: ['Nolodire20', Validators.required]
      }
    )
  }
  login() {
    console.log("login was clicked", this.myForm.value);
    this.loginData = {
      username: this.myForm.value.username,
      password: this.myForm.value.password,
      type: "normal",
    };

    this.loginService.postLogin( this.loginData).subscribe(
      (res) => {
        this.user = res;
        console.log(this.user);
        //call data for  next page
        this.getPendientes("46", this.user.auth_token);
      },
      (error) => {
        console.log('Erreur  found! : ', error);
      }
    );

  }
  anular() {
    console.log("anular");
    this.myForm.controls['username'].setValue('');
    this.myForm.controls['password'].setValue('');

  }
  private getPendientes(projectId: String, auth_token: String){
    this.pendienteService.get(projectId, auth_token).subscribe(
      (res) => {
        this.pendingList = res; 
      },
      (error) => {
        console.log('Erreur  found! : ', error);
      }

    );

  }
}

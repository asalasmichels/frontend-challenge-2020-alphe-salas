export class Pending {
    closed: Boolean;
    closed_points: Number;
    created_date: Date;
    disponibility: Number;
    estimated_finish: String;
    estimated_start: String;
    id: Number;
    modified_date: Date;
    name: String;
    order: Number;
    owner: Number;
    project: Number;
    project_extra_info: extraInfo;
    slug: String;
    total_points: Number;
    user_stories: userStories[]
}
class extraInfo 
{
    id: Number;
    logo_small_url: null;
    name: String;
    slug: String;
}
class AssignedExtraInfo 
{
    big_photo: String;
    full_name_display: String;
    gravatar_id: String;
    id: Number;
    is_active: Boolean;
    photo: String;
    username: String;
}

class StatusExtraInfo 
{
    color: String;
    is_closed: Boolean;
    name: String;
}
class ProjectExtraInfo 
{
    id: Number;
    logo_small_url: String;
    name: String;
    slug: String;
}

class Points 
{
    1: Number;
    2: Number;
    3: Number;
    4: Number;
}
class userStories 
{
    assigned_to: Number;
    assigned_to_extra_info: AssignedExtraInfo;
    blocked_note: String;
    client_requirement: false;
    created_date: Date;
    due_date: Date;
    due_date_reason: String;
    due_date_status: String;
    external_reference: String;
    finish_date: Date;
    id: Number;
    is_blocked: Boolean;
    is_closed: Boolean;
    milestone: Number;
    modified_date: Date;
    points: Points;
    project: 1;
    project_extra_info: ProjectExtraInfo;
    ref: Number;
    sprint_order: Number;
    status: Number;
    status_extra_info: StatusExtraInfo;
    subject: String;
    team_requirement: Boolean;
    total_points: Number;
    version: Number;
}
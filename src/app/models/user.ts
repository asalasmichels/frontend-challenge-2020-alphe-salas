export class User 
    {
        id: Number;
        username: String;
        full_name: String;
        full_name_display: String;
        color: String;
        bio: String;
        lang: String;
        theme: String;
        timezone: String;
        is_active: Boolean;
        photo: String;
        big_photo: String;
        gravatar_id: String;
        roles: String [];
        total_private_projects: Number;
        total_public_projects: Number;
        email: String;
        uuid: String;
        date_joined: Date;
        read_new_terms: Boolean;
        accepted_terms: Boolean;
        max_private_projects: Number;
        max_public_projects: Number;
        max_memberships_private_projects: Number;
        max_memberships_public_projects: Number;
        auth_token: String;
    }
